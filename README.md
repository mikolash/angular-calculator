# Calculator

Simple calculator application written in Angular

## Running the app

Clone the project to your device.
Run `npm install`.
Run `ng serve` for a dev server.
Navigate to `http://localhost:4200/`.
