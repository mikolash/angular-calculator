import { Injectable } from '@angular/core';
import { BehaviorSubject} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CalculatorService {

  private memory1Source = new BehaviorSubject<string>('');
  memory1 = this.memory1Source.asObservable();

  private memory2Source = new BehaviorSubject<string>('');
  memory2 = this.memory2Source.asObservable();

  private memory3Source = new BehaviorSubject<string>('');
  memory3 = this.memory3Source.asObservable();

  private currentSource = new BehaviorSubject<string>('0');
  current = this.currentSource.asObservable();

  private maxLength = 36;
  private result: string;
  private operator: string;
  private lastExpression = '';
  private operatorTaken = false;
  private isNum1Decimal = false;
  private isNum2Decimal = false;

  constructor() {}

  /**
   * @returns Returns true if operation applicable, false otherwise
   */
  operationApplicable(): boolean {
    return (!this.operatorTaken);
  }

  /**
   * @returns Returns true if decimal point applicable, false otherwise
   */
  decimalPointApplicable(): boolean {
    if (!this.isNum1Decimal && !this.operatorTaken || !this.isNum2Decimal && this.operatorTaken) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * @returns {string} Returns current content of active display
   */
  getCurrent(): string {
    return this.currentSource.getValue();
  }

  /**
   * Adds pressed symbol to the active display if possible
   * @param {string} key - pressed symbol
   */
  addKey(key: string): void {
    if (this.currentSource.getValue().length < this.maxLength) {
      // if active display ends with ')' then the key is inserted just before it, otherwise at the end
      if (this.currentSource.getValue().substring
        (this.currentSource.getValue().length - 1, this.currentSource.getValue().length) === ')') {
        this.currentSource.next(this.currentSource.getValue().
          substring(0, this.currentSource.getValue().length - 1).concat(key.concat(')')));
      } else {
        this.currentSource.next(this.currentSource.getValue().concat(key));
      }

      if (key === '.') {
        if (this.operatorTaken) {
          this.isNum2Decimal = true;
        } else {
          this.isNum1Decimal = true;
        }
      } else if (key === '/' || key === '*' || key === '-' || key === '+') {
        this.operatorTaken = true;
        this.operator = key;
      }
    }
  }

  /**
   * Saves the last expression
   */
  setLastExpression(): void {
    if (this.operatorTaken) {
      this.lastExpression = this.currentSource.getValue().substring(this.currentSource.
      getValue().indexOf(this.operator), this.currentSource.getValue().length);
    } else {
      this.lastExpression = '';
    }
  }

  /**
   * Carries out operations when equals sign is pressed
   * Calculates result, saves the result in memory and restarts the calculator
   */
  equals(): void {
    // mark as math error if necessary
    if (this.operatorTaken && (this.currentSource.getValue().substring(this.currentSource.
      getValue().indexOf(this.operator), this.currentSource.getValue().length) === '/0' ||
      this.currentSource.getValue().substring(this.currentSource.getValue().indexOf(this.operator),
        this.currentSource.getValue().length) === '/(-0)')) {
      this.result = 'Math error';
    } else {
      this.result = '' + eval(this.currentSource.getValue());
    }

    // rounds long decimal numbers if necessary
    if (this.result.includes('.') && (this.result.length - this.result.indexOf('.') > 8)) {
      this.roundResult();
    }

    // set the values of corresponding variables
    this.currentSource.next(this.currentSource.getValue().concat(' = ').concat(this.result));
    this.memory3Source.next(this.memory2Source.getValue());
    this.memory2Source.next(this.memory1Source.getValue());
    this.memory1Source.next(this.currentSource.getValue());

    // restart the state of the calculator
    this.currentSource.next(this.result);
    this.operator = '';
    this.operatorTaken = false;
    this.isNum2Decimal = false;
    this.isNum1Decimal = this.result.includes('.');
  }

  /**
   * Processes situation when equal sign is pressed for second time in row
   */
  secondEquals(): void {
    if (this.currentSource.getValue() !== 'Math error') {
      this.currentSource.next(this.currentSource.getValue().concat(this.lastExpression));
    }
  }

  /**
   * Replaces the current operation symbol with new one
   * @param {string} key - Operation symbol to replace the current operation
   */
  replaceOperation(key: string) {
    this.currentSource.next(this.currentSource.getValue().
      substring(0, this.currentSource.getValue().length - 1).concat(key));
  }

  /**
   * Replaces current content of active display
   * @param {string} current - String to replace the current content of active display
   */
  replaceCurrent(current: string): void {
    this.currentSource.next(current);
    this.isNum1Decimal = false;
  }

  /**
   * Negates the current content of active display
   */
  negateCurrent(): void {
    // check if operator is already taken
    if (!this.operatorTaken) {
      // delete the minus sign if expression is negative
      if (this.currentSource.getValue().substring(0, 1) === '-') {
        this.currentSource.next(this.currentSource.getValue().substring(1, this.currentSource.getValue().length + 1));
      } else if (this.currentSource.getValue() !== '0') {
        // add minus sign if expression is positive
        this.currentSource.next('-'.concat(this.currentSource.getValue()));
      }
      // if the operator is already taken then:
    } else {
      // delete the brackets and minus sign if the number is negative
      if (this.currentSource.getValue().substring
          (this.currentSource.getValue().indexOf(this.operator) + 1,
          this.currentSource.getValue().indexOf(this.operator) + 3) === '(-') {
        this.currentSource.next(this.currentSource.getValue().
          substring(0, this.currentSource.getValue().indexOf(this.operator) + 1).
          concat(this.currentSource.getValue().substring(this.currentSource.getValue().
          indexOf(this.operator) + 3, this.currentSource.getValue().length - 1)));
      } else {
        // add brackets and minus sign if the number is positive
         this.currentSource.next(this.currentSource.getValue().substring
          (0, this.currentSource.getValue().indexOf(this.operator) + 1).
         concat('(-'.concat(this.currentSource.getValue().substring
          (this.currentSource.getValue().indexOf(this.operator) + 1, this.currentSource.getValue().length + 1).concat(')'))));
      }
    }
  }

  /**
   * Clears the content of active display and sets it to 0
   */
  clear(): void {
    this.currentSource.next('0');
    this.operatorTaken = false;
    this.isNum1Decimal = false;
    this.isNum2Decimal = false;
  }

  /**
   * Round the result of a long decimal number - fix precision of calculations with decimal numbers
   */
  private roundResult(): void {
    const shiftPoints = this.result.length - this.result.indexOf('.') - 3;
    let resultNumber = +this.result;
    resultNumber = (Math.round(resultNumber * Math.pow(10, shiftPoints))) / (Math.pow(10, shiftPoints));
    this.result = '' + resultNumber;
  }
}
