import { Component, OnInit } from '@angular/core';
import { CalculatorService} from '../calculator.service';

@Component({
  selector: 'app-displays',
  templateUrl: './displays.component.html',
  styleUrls: ['./displays.component.css']
})
export class DisplaysComponent implements OnInit {

  memory1: string;
  memory2: string;
  memory3: string;
  current: string;

  constructor(private calculatorService: CalculatorService) { }

  /**
   * Subscribe values to display from calculator
   */
  ngOnInit() {
    this.calculatorService.current.subscribe(current => this.current = current);
    this.calculatorService.memory1.subscribe(memory1 => this.memory1 = memory1);
    this.calculatorService.memory2.subscribe(memory2 => this.memory2 = memory2);
    this.calculatorService.memory3.subscribe(memory3 => this.memory3 = memory3);
  }

}
