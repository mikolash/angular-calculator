import { Component, OnInit } from '@angular/core';
import { ReadKeyService } from '../read-key.service';

@Component({
  selector: 'app-controller',
  templateUrl: './controller.component.html',
  styleUrls: ['./controller.component.css'],
})
export class ControllerComponent implements OnInit {

  constructor(public readKeyService: ReadKeyService) { }

  ngOnInit() {
  }

  readKey(key): void {
    this.readKeyService.readKey(key);
  }
}
