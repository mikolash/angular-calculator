export class Expression {
  num1: number;
  num2: number;
  isNum1Decimal: boolean;
  isNum2Decimal: boolean;
  operator: string;
  operatorTaken: boolean;

  getExpression(): string {
    if (!this.num1) {
      return '0';
    }
  }
}
