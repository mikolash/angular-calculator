import { TestBed, inject } from '@angular/core/testing';

import { ReadKeyService } from './read-key.service';

describe('ReadKeyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReadKeyService]
    });
  });

  it('should be created', inject([ReadKeyService], (service: ReadKeyService) => {
    expect(service).toBeTruthy();
  }));
});
