import { Injectable } from '@angular/core';
import { CalculatorService} from './calculator.service';

@Injectable({
  providedIn: 'root'
})
export class ReadKeyService {

  private isLastOperation = false;
  private isLastEquals = false;

  constructor(private calculator: CalculatorService) { }

  /**
   * Read a key when pressed
   * @param key - Key pressed
   */
  readKey(key): void {
    if (!isNaN(key)) {
      this.readNum(key);
    } else if (key === '.') {
      this.readDot();
    } else if (key === 'ac') {
      this.readAc();
    } else if (key === 'neg') {
      this.readNeg();
    } else if (key === '=') {
      this.readEquals();
    } else {
      this.readOperation(key);
    }
  }

  /**
   * Reads number symbol when pressed
   * @param {string} key - Number key pressed
   */
  private readNum(key: string): void {
    if (this.calculator.getCurrent() === '0' || this.calculator.getCurrent() === 'Math error'
      || this.isLastEquals) {
      this.calculator.replaceCurrent(key);
    } else {
      this.calculator.addKey(key);
    }
    this.isLastOperation = false;
    this.isLastEquals = false;
  }

  /**
   * Reads dot symbol when pressed
   */
  private readDot(): void {
    if (this.calculator.decimalPointApplicable() || this.isLastEquals) {
      if (this.isLastEquals || this.calculator.getCurrent() === 'Math error') {
        this.calculator.replaceCurrent('0');
      } else if (this.isLastOperation) {
        this.calculator.addKey('0');
      }
      this.calculator.addKey('.');
      this.isLastOperation = false;
      this.isLastEquals = false;
    }
  }

  /**
   * Reads AC key when pressed
   */
  private readAc(): void {
    this.calculator.clear();
    this.isLastOperation = false;
    this.isLastEquals = false;
  }

  /**
   * Reads +/- key when pressed
   */
  private readNeg(): void {
    if (this.calculator.getCurrent() !== 'Math error') {
      this.calculator.negateCurrent();
      this.isLastEquals = false;
    }
    this.isLastEquals = false;
    this.isLastOperation = false;
  }

  /**
   * Read = symbol when pressed
   */
  private readEquals(): void {
    if (this.calculator.getCurrent() !== 'Math error') {
      if (this.isLastEquals) {
        this.calculator.secondEquals();
      } else {
        this.calculator.setLastExpression();
      }
      this.calculator.equals();
      this.isLastEquals = true;
    }
  }

  /**
   * Reads operation symbol when pressed
   * @param key - Operation key pressed
   */
  private readOperation(key): void {
    if (this.calculator.getCurrent() !== 'Math error') {
      if (this.calculator.operationApplicable()) {
        this.calculator.addKey(key);
        this.isLastOperation = true;
        this.isLastEquals = false;
      } else if (this.isLastOperation) {
        this.calculator.replaceOperation(key);
      } else {
        this.readEquals();
        this.readOperation(key);
      }
    }

  }
}
